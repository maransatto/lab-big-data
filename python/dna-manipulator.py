import json

class DNAManipulator:
    def __init__(self, model):
        self.model = model
        self.RNA = []
    
    def invert(self, var):
        return var[::-1]

    def makeRNA(self, sequence, position=0):
        if position == len(sequence):
            return "".join(self.RNA)
        if sequence[position] in self.model:
            self.RNA.append(self.model[sequence[position]])
            return self.makeRNA(sequence, position+1)

    def removeBreakLine(self, content):
        return content.replace('\n', ' ').replace('\r', '')

    def matchSequences(self, first, second):
        fist = self.removeBreakLine(first)
        second = self.removeBreakLine(second)
        matched = 0
        unmatched = 0
        total_range = range(len(first)) if len(first) < len(second) else range(len(second))
        for i in total_range:
            matched     = matched+1     if first[i] == second[i] else matched
            unmatched   = unmatched+1   if first[i] != second[i] else unmatched

        return {
            'sequence_size' : len(first),
            'homologous' : {
                'matched_quantity': matched,
                'matched_porcentage': (matched*100.0) / len(total_range),
            },
            'mutation' : {
                'unmatched_quantity': unmatched,
                'unmatched_porcentage': (unmatched*100.0) / len(total_range),
            }
        }

    def getFileContent(self, path):
        file = open(path, "r")
        return file.read()

RNAwithTemplateStrand = {
    "A":"U",
    "T":"A",
    "C":"G",
    "G":"C"
}
RNAwithCodingStrand = {
    "T":"U",
    "A":"A",
    "G":"G",
    "C":"C"
}

# templateStrand = DNAManipulator(RNAwithTemplateStrand)
# print(templateStrand.makeRNA("TACTAGA"))

# codingStrand = DNAManipulator(RNAwithCodingStrand)
# print(codingStrand.makeRNA("ATGATCT"))

dna = DNAManipulator(RNAwithTemplateStrand)
file1_content = dna.getFileContent('arabidopsis-thaliana.txt')
file2_content = dna.getFileContent('homo-sapiens.txt')
print(json.dumps(dna.matchSequences(file1_content, file2_content), indent=4, sort_keys=True))
